
class InformationModel {

  final int id;
  final String title;
  final String description;

  InformationModel({
    required this.id,
    required this.title,
    required this.description
  });

  factory InformationModel.fromJson(Map<String,dynamic> json)=>
      InformationModel(
          id: json['id'],
          title: json['title'],
          description: json['description']
      );

}