import 'package:admin/core/core_components/app_text_form_feild.dart';
import 'package:admin/core/core_components/submit_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class CreateInformationComponent extends StatelessWidget {

  CreateInformationComponent({Key? key, required this.onTap, required this.titleController, required this.descriptionController}) : super(key: key);
  final VoidCallback onTap;
  final TextEditingController titleController;
  final TextEditingController descriptionController;
  final GlobalKey<FormState> _key = GlobalKey<FormState>();


  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        padding: EdgeInsets.all(2.5.w),
        width: 50.w,
        child: Form(
          key: _key,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 5.w,
              ),
              AppTextFormField(
                hint: 'title',
                controller: titleController,
                validator: (val) {
                  if(val == null||val.isEmpty) {
                    return 'required';
                  }
                },
              ),
              SizedBox(
                height: 5.w,
              ),
              AppTextFormField(
                hint: 'description',
                maxLines: 3,
                controller: descriptionController,
                validator: (val) {
                  if(val == null||val.isEmpty) {
                    return 'required';
                  }
                },
              ),
              SizedBox(
                height: 5.w,
              ),
              SubmitButton(
                height: 4.w,
                width: 30.w,
                titleSize: 8.spa,
                onTap: _submit,
              ),
              SizedBox(
                height: 5.w,
              ),
            ],
          ),
        ),
      ),
    );
  }


  void _submit(){
    if(!(_key.currentState?.validate() ?? false)){
      return;
    }
    onTap();
    Get.back();
    descriptionController.text = '';
    titleController.text = '';
  }
}
