import 'package:admin/core/core_components/state_component.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import '../../../../core/consts/color_const.dart';
import '../../../../core/core_components/app_drwaer.dart';
import '../../../../core/core_components/app_text_form_feild.dart';
import '../../../../core/core_components/submit_button.dart';
import '../controllers/information/information_binding.dart';
import '../controllers/information/information_controller.dart';

class InformationScreen extends GetView<InformationController> {
  const InformationScreen({super.key});

  static const name = '/info';
  static final GetPage page = GetPage(
      name: name,
      page: () => const InformationScreen(),
      binding: InformationBindings(),

  );

  @override
  Widget build(BuildContext context) {
    MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Information'),
      ),
      body: Row(
        children: [
          Expanded(
              child: AppDrawer(
                selected: 0,
              )
          ),
          Expanded(
              flex: 3,
              child: GetBuilder<InformationController>(
                builder: (_) =>
                    StatusComponent(
                        state: controller.state,
                        onSuccess: (_,state) =>ListView.builder(
                            itemCount: controller.data.length,
                            itemBuilder: (_,i) =>
                                ListTile(
                                  title: Text(controller.data[i].title),
                                  subtitle: Text(controller.data[i].description),
                                )
                        )
                    ),
              ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add_box_rounded,color: AppColors.white,),
        onPressed: controller.create,
      ),
    );
  }
}
