import 'package:get/get.dart';
import 'information_controller.dart';

class InformationBindings extends Bindings {
  @override
  void dependencies() {
    Get.put(InformationController());
  }
}
