import 'package:admin/core/base_controllers/load_data_controller.dart';
import 'package:admin/modules/information/data/model/information_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../core/core_components/pop_up.dart';
import '../../../../../core/handler/handler.dart';
import '../../../data/data_source/information_data_source.dart';
import '../../components/create_information_component.dart';

class InformationController extends LoadDataController<List<InformationModel>> {
  @override
  Future<List<InformationModel>> callApi() =>
      InformationDataSource.getInformation();

  final TextEditingController titleController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();


  void _createCall()async{
    showLoader();
    var result = await handle(() => InformationDataSource.createInformation(
        titleController.text,
        descriptionController.text
    ));
    Get.back();
    result.fold(
            (failState) => showSnackBar(failState.message),
            (successState) => loadData()
    );
  }

  void create(){
    Get.dialog(CreateInformationComponent(
      onTap: _createCall,
      descriptionController: descriptionController,
      titleController: titleController,
    ));
  }



}
