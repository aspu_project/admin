
import '../../../../core/consts/api_const.dart';
import '../../../../core/network/network_helper.dart';
import '../../../../core/storage/storage_handler.dart';

class AuthDataSource {

  static Future<void> login(String email,String password) async{
    var response = await NetworkHelper().post(
      ApiConst.login,
      body: {
        'email':email,
        'password':password
      }
    );
    await StorageHandler().setToken(response.data['data']['token']);
  }

}
