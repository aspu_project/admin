import 'package:admin/core/consts/assets_const.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import '../../../../core/consts/color_const.dart';
import '../../../../core/core_components/app_text_form_feild.dart';
import '../../../../core/core_components/submit_button.dart';
import '../components/login_clipper.dart';
import '../controllers/login/login_binding.dart';
import '../controllers/login/login_controller.dart';
import '../controllers/login/login_middleware.dart';

class LoginScreen extends GetView<LoginController> {
  const LoginScreen({super.key});

  static const name = '/';
  static final GetPage page = GetPage(
      name: name,
      page: () => const LoginScreen(),
      binding: LoginBindings(),
      middlewares: [LoginMiddleware()]);

  @override
  Widget build(BuildContext context) {
    MediaQuery.of(context);
    return Scaffold(
        body: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded  (
            child: Align(
                alignment: Alignment.center,
                child: Image.asset(
                  ImageConst.aspu,
                  width: 25.w,
                )
            )
        ),
            Expanded(
              child: SingleChildScrollView(
                padding: EdgeInsets.all(2.5.w),
                child: SizedBox(
                  width: double.infinity,
                  child: Form(
                    key: controller.formKey,
                    child: Column(
                      children: [
                        Text(
                          "Welcome In ASPU Admin Panel",
                          style: TextStyle(
                            fontSize: 14.spa,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        Divider(
                          height: 10.w,
                          color: AppColors.lightGrey,
                        ),
                        SizedBox(
                          height: 2.5.w,
                        ),
                        AppTextFormField(
                          hint: 'Email',
                          icon: Icon(Icons.email),
                          controller: controller.emailController,
                          validator: (val) {
                            if (val == null || val.isEmpty)
                              return 'email is required';
                            if (!val.isEmail) return 'must be an email text';
                          },
                        ),
                        SizedBox(
                          height: 2.5.w,
                        ),
                        AppTextFormField(
                          hint: 'password',
                          icon: Icon(Icons.lock),
                          isPass: true,
                          controller: controller.passController,
                          validator: (val) {
                            if (val == null || val.isEmpty)
                              return 'password is required';
                            if (val.length < 6)
                              return 'must be 6 characters at least';
                          },
                        ),
                        SizedBox(
                          height: 2.5.w,
                        ),
                        SubmitButton(
                          label: 'Login',
                          onTap: controller.login,
                          height: 6.w,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
    ));
  }
}
