import 'package:admin/modules/information/data/model/information_model.dart';
import 'package:admin/modules/information/presentation/screens/information_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../core/storage/storage_handler.dart';

class LoginMiddleware extends GetMiddleware {


  @override
  RouteSettings? redirect(String? route) {
    if(StorageHandler().hasToken){
      return RouteSettings(
        name: InformationScreen.name
      );
    }
  }
}