import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../core/core_components/pop_up.dart';
import '../../../../../core/handler/handler.dart';
import '../../../../information/presentation/screens/information_screen.dart';
import '../../../data/data_source/auth_data_source.dart';

class LoginController extends GetxController {

  final formKey = GlobalKey<FormState>();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();

  void login() async {
    if(!(formKey.currentState?.validate() ?? true)){
      showSnackBar('error , invalid input');
      return;
    }
    showLoader();
    var state = await handle(() => AuthDataSource.login(emailController.text, passController.text));
    Get.back();
    state.fold(
            (failState) => showSnackBar(failState.message),
            (successState) => Get.offAllNamed(InformationScreen.name)
    );
  }
}
