import 'exam_model.dart';

class StudentModel {
  final int id;
  final String name;
  final String email;
  final String role;
  final int studentId;
  final int year;
  final int hours;
  final num gpa;
  final DateTime startedAt;
  final List<ExamModel> exams;

  StudentModel({
    required this.id,
    required this.name,
    required this.email,
    required this.role,
    required this.studentId,
    required this.year,
    required this.hours,
    required this.gpa,
    required this.startedAt,
    required this.exams,
  });

  factory StudentModel.fromJson(Map<String, dynamic> json) => StudentModel(
      id: json['user']["id"],
      name: json['user']["name"],
      email: json['user']["email"],
      role: json['user']["role"],
      studentId: json["id"],
      year: json["year"],
      hours: json["hours"],
      gpa: json["gpa"],
      startedAt: DateTime.parse(json["started_at"]),
      exams: (json['exams'] as List?)?.map((e) => ExamModel.fromJson(e)).toList() ?? <ExamModel>[]
  );

}