
import '../../../../core/consts/api_const.dart';
import '../../../../core/network/network_helper.dart';
import '../../../../core/storage/storage_handler.dart';
import '../model/student_model.dart';

class StudentsDataSource {

  static Future<List<StudentModel>> getStudents()async{
    var response = await NetworkHelper().get(ApiConst.students);
    List models = response.data['data'];
    return models.map((e) =>StudentModel.fromJson(e)).toList();
  }

  static Future<StudentModel> getById(int id)async{
    var response = await NetworkHelper().get(ApiConst.getStudentsById(id));
    return StudentModel.fromJson(response.data['data']);
  }

}
