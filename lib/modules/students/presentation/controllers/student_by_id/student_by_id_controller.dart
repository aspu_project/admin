import 'package:admin/core/base_controllers/load_data_controller.dart';
import 'package:admin/modules/information/data/model/information_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../core/core_components/pop_up.dart';
import '../../../../../core/handler/handler.dart';
import '../../../data/data_source/students_data_source.dart';
import '../../../data/model/student_model.dart';

class StudentByIdController extends LoadDataController<StudentModel> {

  final StudentModel model;


  StudentByIdController(this.model);

  @override
  Future<StudentModel> callApi() =>
      StudentsDataSource.getById(model.id);





}
