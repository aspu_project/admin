import 'package:get/get.dart';
import 'student_by_id_controller.dart';

class StudentsByIdBindings extends Bindings {
  @override
  void dependencies() {
    Get.put(StudentByIdController(Get.arguments));
  }
}
