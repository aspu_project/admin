import 'package:admin/core/core_components/state_component.dart';
import 'package:admin/modules/students/presentation/controllers/students/students_controller.dart';
import 'package:admin/modules/students/presentation/screens/student_by_id_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import '../../../../core/consts/color_const.dart';
import '../../../../core/core_components/app_drwaer.dart';
import '../../../../core/core_components/app_text_form_feild.dart';
import '../../../../core/core_components/submit_button.dart';
import '../controllers/students/students_binding.dart';

class StudentsScreen extends GetView<StudentController> {
  const StudentsScreen({super.key});

  static const name = '/students';
  static final GetPage page = GetPage(
      name: name,
      page: () => const StudentsScreen(),
      binding: StudentsBindings(),

  );

  @override
  Widget build(BuildContext context) {
    MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Students'),
      ),
      body: Row(
        children: [
          const Expanded(
              child: AppDrawer(
                selected: 2,
              )
          ),
          Expanded(
              flex: 3,
              child: GetBuilder<StudentController>(
                builder: (_) =>
                    StatusComponent(
                        state: controller.state,
                        onSuccess: (_,state) =>ListView.builder(
                            itemCount: controller.data.length,
                            itemBuilder: (_,i) =>
                                ListTile(
                                  title: Text(controller.data[i].name),
                                  subtitle: Text('year : ${controller.data[i].year}'),
                                  onTap: ()=>StudentsByIdScreen.navigate(controller.data[i]),
                                )
                        )
                    ),
              ),
          )
        ],
      ),
    );
  }
}
