import 'package:admin/modules/students/data/model/student_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:intl/intl.dart';
import '../../../../core/consts/color_const.dart';
import '../../../../core/core_components/state_component.dart';
import '../components/exam_view.dart';
import '../controllers/student_by_id/student_by_id_binding.dart';
import '../controllers/student_by_id/student_by_id_controller.dart';
import 'package:syncfusion_flutter_charts/charts.dart';


class StudentsByIdScreen extends GetView<StudentByIdController> {
  const StudentsByIdScreen({Key? key}) : super(key: key);

  static const _name = '/student_screen';
  static final GetPage page = GetPage(
      name: _name, page: () => const StudentsByIdScreen(),
      binding: StudentsByIdBindings()
  );

  static void navigate(StudentModel model) =>
      Get.toNamed(_name, arguments: model);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(controller.model.name),
      ),
      body: GetBuilder<StudentByIdController>(
        builder: (_) => StatusComponent(
            state: controller.state,
            onSuccess: (_, state) => Padding(
                  padding: EdgeInsets.all(2.5.w),
                  child: DefaultTextStyle(
                    style: TextStyle(
                        color: AppColors.lightGrey,
                        fontSize: 14.spa
                    ),
                    child: Padding(
                      padding:EdgeInsets.all(1.w),
                      child: CustomScrollView(
                        slivers: [
                          SliverList(
                              delegate: SliverChildListDelegate(
                                  [
                                    Row(
                                      children: [
                                        Expanded(
                                          child: SizedBox(
                                            height: 30.w,
                                            width: 30.w,
                                            child: SfCircularChart(
                                              palette: const [
                                                AppColors.blue,
                                                AppColors.darkColor,
                                              ],
                                              series: <DoughnutSeries<num, String>>[
                                                DoughnutSeries(
                                                  dataSource: [controller.data.gpa/4,1-controller.data.gpa/4],
                                                  xValueMapper: (value,__) => '',
                                                  yValueMapper: (value,__) => value,
                                                  dataLabelMapper: (value,i) => i == 0 ? controller.data.gpa.toString():'GPA',
                                                  dataLabelSettings: DataLabelSettings(isVisible: true),
                                                ),
                                              ],

                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Container(
                                            alignment: Alignment.center,
                                            padding: EdgeInsets.symmetric(
                                                vertical: 2.5.w
                                            ),
                                            child: Text(
                                                controller.data.name,
                                                style: TextStyle(
                                                    fontSize: 18.spa,
                                                    fontWeight: FontWeight.bold
                                                )
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    const Divider(),
                                    ListTile(
                                      leading: const Icon(Icons.date_range),
                                      title: Text('Started at : ${DateFormat("yyyy - MMM - dd").format(controller.data.startedAt)}'),
                                    ),
                                    ListTile(
                                      leading: const Icon(Icons.school),
                                      title: Text('Studying year : ${controller.data.year}'),
                                    ),
                                    ListTile(
                                      leading: const Icon(Icons.schedule),
                                      title: Text('Passed Hours : ${controller.data.hours}'),
                                    ),
                                    ListTile(
                                      leading: const Icon(Icons.bar_chart),
                                      title: Text('GPA : ${controller.data.gpa}/4'),
                                    ),
                                    ListTile(
                                      leading: const Icon(Icons.email),
                                      title: Text('Student Email : ${controller.data.email}'),
                                    ),
                                    const Divider(),
                                    const ListTile(
                                      leading: Icon(Icons.text_increase_sharp),
                                      title: Text('Exams Results : '),
                                    ),
                                  ]
                              )
                          ),
                          SliverList(
                              delegate: SliverChildBuilderDelegate(
                                      (_,i) => ExamView(controller.data.exams[i]),
                                  childCount: controller.data.exams.length
                              )
                          )
                        ],
                      ),
                    ),
                  )
                )),
      ),
    );
  }


}
