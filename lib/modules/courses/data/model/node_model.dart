import 'course_model.dart';

class NodeModel {
  final int id;
  final List<CourseModel> parentsAnd;

  NodeModel({
    required this.id,
    required this.parentsAnd,
  });

  factory NodeModel.fromJson(Map<String, dynamic> json) => NodeModel(
    id: json["id"],
    parentsAnd: List<CourseModel>.from(json["parents_and"].map((x) => CourseModel.fromJson(x))),
  );

}