


import '../../../../core/consts/api_const.dart';
import '../../../../core/network/network_helper.dart';
import '../model/course_model.dart';

class CoursesDataSource {


  static Future<List<CourseModel>> getAll()async{
    var response = await NetworkHelper().get(ApiConst.courses);
    List courses = response.data['data'];
    return courses.map((e) => CourseModel.fromJson(e)).toList();
  }



  static Future<CourseModel> getById(int id)async{
    var response = await NetworkHelper().get(ApiConst.getCoursesById(id));
    return CourseModel.fromJson(response.data['data']);
  }


}