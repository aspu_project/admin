import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../../core/consts/color_const.dart';
import '../../../../core/core_components/state_component.dart';
import '../../data/model/course_model.dart';
import '../components/node_view.dart';
import '../controller/course/course_binding.dart';
import '../controller/course/course_controller.dart';

class CourseScreen extends GetView<CourseController> {
  const CourseScreen({Key? key}) : super(key: key);

  static const _name = '/course_screen';
  static final GetPage page = GetPage(
      name: _name, page: () => const CourseScreen(), binding: CourseBinding());

  static void navigate(CourseModel model) =>
      Get.toNamed(_name, arguments: model);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(controller.model.name),
      ),
      body: GetBuilder<CourseController>(
        builder: (_) => StatusComponent(
            state: controller.state,
            onSuccess: (_, state) => Padding(
                  padding: EdgeInsets.all(2.5.w),
                  child: CustomScrollView(
                    slivers: [
                      SliverList(
                          delegate: SliverChildListDelegate([
                        _keyValue('Course Name : ', controller.model.name),
                        _keyValue('Total Hours : ', controller.model.hours.toString()),
                        const Divider(),
                        _keyValue('Required Courses :', controller.data.parentsOr!.isEmpty ? 'No courses':'')
                        ])
                      ),
                      SliverList(
                          delegate: SliverChildBuilderDelegate(
                            (context, i) => NodeView(controller.data.parentsOr![i]),
                            childCount: controller.data.parentsOr!.length
                          )
                      )
                    ],
                  ),
                )),
      ),
    );
  }

  Widget _keyValue(String key, String value) => ListTile(
        title: Text.rich(
          TextSpan(
              text: key,
              style: const TextStyle(color: AppColors.white),
              children: [
                TextSpan(
                  text: value,
                  style: const TextStyle(color: AppColors.lightBlue),
                )
              ]),
        ),
      );
}
