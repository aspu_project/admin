import 'package:get/get.dart';

import '../../../../../core/base_controllers/load_data_controller.dart';
import '../../../data/data_source/courses_data_source.dart';
import '../../../data/model/course_model.dart';

class AllCoursesController extends LoadDataController<List<CourseModel>>{
  @override
  Future<List<CourseModel>> callApi() =>
      CoursesDataSource.getAll();

}