import 'package:get/get.dart';

import '../../../../../core/base_controllers/load_data_controller.dart';
import '../../../data/data_source/courses_data_source.dart';
import '../../../data/model/course_model.dart';

class CourseController extends LoadDataController<CourseModel>{

  final CourseModel model;


  CourseController(this.model);

  @override
  Future<CourseModel> callApi() =>
      CoursesDataSource.getById(model.id);

}