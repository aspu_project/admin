import 'package:flutter/material.dart';

import '../../data/model/node_model.dart';
import 'course_view.dart';

class NodeView extends StatelessWidget {
  final NodeModel model;
  const NodeView(this.model,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      children: [
        const ListTile(
          title: Text('Group'),
        ),
        ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: model.parentsAnd.length,
            itemBuilder: (_,i)=> CourseView(model.parentsAnd[i])
        )
      ],
    );
  }
}
