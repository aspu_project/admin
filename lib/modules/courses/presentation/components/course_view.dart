import 'package:flutter/material.dart';

import '../../data/model/course_model.dart';
import '../screens/course_screen.dart';

class CourseView extends StatelessWidget {
  final CourseModel model;
  final bool navigate;
  const CourseView(this.model,{Key? key,this.navigate = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: const Icon(Icons.book_outlined),
      title: Text(model.name),
      subtitle: Text('total hours : ${model.hours}'),
      onTap:navigate? ()=> CourseScreen.navigate(model):null,
    );
  }
}
