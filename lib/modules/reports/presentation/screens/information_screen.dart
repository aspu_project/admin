import 'package:admin/core/core_components/state_component.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../core/core_components/app_drwaer.dart';
import '../controllers/reports/report_binding.dart';
import '../controllers/reports/report_controller.dart';

class ReportsScreen extends GetView<ReportsController> {
  const ReportsScreen({super.key});

  static const name = '/reports';
  static final GetPage page = GetPage(
      name: name,
      page: () => const ReportsScreen(),
      binding: ReportsBindings(),

  );

  @override
  Widget build(BuildContext context) {
    MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Reports'),
      ),
      body: Row(
        children: [
          const Expanded(
              child: AppDrawer(
                selected: 3,
              )
          ),
          Expanded(
              flex: 3,
              child: GetBuilder<ReportsController>(
                builder: (_) =>
                    StatusComponent(
                        state: controller.state,
                        onSuccess: (_,state) =>ListView.separated(
                            itemCount: controller.data.length,
                            itemBuilder: (_,i) =>
                                ListTile(
                                  title: Text(
                                    """
Question : ${controller.data[i].question}
Answer : ${controller.data[i].answer}
                                    """
                                  ),
                                  subtitle: Text('Reason : ${controller.data[i].reason}'),
                                ),
                            separatorBuilder: (_,i) =>
                              const Divider(),
                        ),
                    ),
              ),
          )
        ],
      ),
    );
  }
}
