import 'package:admin/core/base_controllers/load_data_controller.dart';
import 'package:admin/modules/information/data/model/information_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../core/core_components/pop_up.dart';
import '../../../../../core/handler/handler.dart';
import '../../../data/data_source/reports_data_source.dart';
import '../../../data/model/report_model.dart';

class ReportsController extends LoadDataController<List<ReportModel>> {
  @override
  Future<List<ReportModel>> callApi() =>
      ReportsDataSource.getReports();




}
