import 'package:get/get.dart';
import 'report_controller.dart';

class ReportsBindings extends Bindings {
  @override
  void dependencies() {
    Get.put(ReportsController());
  }
}
