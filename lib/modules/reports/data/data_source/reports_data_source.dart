
import '../../../../core/consts/api_const.dart';
import '../../../../core/network/network_helper.dart';
import '../../../../core/storage/storage_handler.dart';
import '../model/report_model.dart';

class ReportsDataSource {

  static Future<List<ReportModel>> getReports()async{
    var response = await NetworkHelper().get(ApiConst.report);
    List models = response.data['data'];
    return models.map((e) =>ReportModel.fromJson(e)).toList();
  }



}
