
class ReportModel {

  final int id;
  final String reason;
  final String question;
  final String answer;

  ReportModel({
    required this.id,
    required this.reason,
    required this.answer,
    required this.question,
  });

  factory ReportModel.fromJson(Map<String,dynamic> json)=>
      ReportModel(
          id: json['id'],
          question: json['message']['question'],
          answer: json['message']['answer'],
          reason: json['reason'],
      );

}