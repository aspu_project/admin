import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../consts/assets_const.dart';


class AppLoader extends StatelessWidget {
  const AppLoader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LottieBuilder.asset(LottieConst.loader);
  }
}

class MessageLoader extends StatelessWidget {
  const MessageLoader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LottieBuilder.asset(LottieConst.messageLoader);
  }
}
