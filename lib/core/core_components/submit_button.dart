import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../consts/color_const.dart';

class SubmitButton extends StatelessWidget {
  final Color? color;
  final Color? fontColor;
  final String? label;
  final VoidCallback? onTap;
  final EdgeInsets? margin;
  final BorderRadius? borderRadius;
  final double? width;
  final double? height;
  final double? titleSize;
  final Widget? child;

  const SubmitButton({
    Key? key,
    this.color,
    this.fontColor,
    this.label,
    this.onTap,
    this.margin,
    this.borderRadius,
    this.width,
    this.height,
    this.child,
    this.titleSize
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: height ?? 8.w,
        margin: margin,
        width: width ?? double.infinity,
        decoration: BoxDecoration(
          color: color ?? AppColors.blue,
          borderRadius: borderRadius ?? BorderRadius.circular(2.5.w),
        ),
        alignment: Alignment.center,
        child: child ??
            Text(
              label ?? 'Submit'.tr,
              style: TextStyle(
                fontSize: titleSize ?? 10.spa,
                fontWeight: FontWeight.bold,
                color: fontColor ?? Colors.white,
              ),
            ),
      ),
    );
  }
}
