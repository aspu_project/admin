import 'package:admin/core/consts/color_const.dart';
import 'package:admin/core/core_components/pop_up.dart';
import 'package:admin/core/storage/storage_handler.dart';
import 'package:admin/modules/auth/presentation/screens/login_screen.dart';
import 'package:admin/modules/information/presentation/screens/information_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../modules/courses/presentation/screens/all_courses_screen.dart';
import '../../modules/reports/presentation/screens/information_screen.dart';
import '../../modules/students/presentation/screens/students_screen.dart';

class AppDrawer extends StatelessWidget {
  const AppDrawer({Key? key, required this.selected}) : super(key: key);
  final int selected;

  final List<_DrawerObject> _pages = const [
    _DrawerObject(
        icon: Icon(Icons.info),
        title: 'Information',
        route: InformationScreen.name),
    _DrawerObject(
        icon: Icon(Icons.school),
        title: 'Courses',
        route: AllCoursesScreen.name
    ),
    _DrawerObject(
        icon: Icon(Icons.person_rounded),
        title: 'Students',
        route: StudentsScreen.name
    ),
    _DrawerObject(
        icon: Icon(Icons.report_outlined),
        title: 'Reports',
        route: ReportsScreen.name
    ),
    _LogoutObject()
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.darkColor.withOpacity(0.35),
      child: ListView.builder(
        itemCount: _pages.length,
        itemBuilder: (_, i) => _DrawerButton(
          object: _pages[i],
          selected: i == selected,
        ),
      ),
    );
  }
}

class _DrawerObject {
  final Icon icon;
  final String title;
  final String route;

  const _DrawerObject(
      {required this.icon, required this.title, required this.route});

  void onTap() => Get.offNamed(route);
}

class _LogoutObject extends _DrawerObject {

  const _LogoutObject():super(icon:const Icon(Icons.logout_rounded),title: 'Logout',route: '');

  @override
  void onTap() async{
    showLoader();
    await StorageHandler().removeToken();
    Get.offAllNamed(LoginScreen.name);
  }
}

class _DrawerButton extends StatelessWidget {
  const _DrawerButton({Key? key, required this.object, required this.selected})
      : super(key: key);
  final _DrawerObject object;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    var borderRadius = BorderRadius.circular(1.w);
    return Container(
        margin: EdgeInsets.all(1.w).copyWith(bottom: 0),
        decoration: BoxDecoration(
            color:selected ? context.theme.scaffoldBackgroundColor:null,
            borderRadius: borderRadius
        ),
        child: ListTile(
          shape: RoundedRectangleBorder(
            borderRadius: borderRadius
          ),
          title: Text(object.title),
          leading: object.icon,
          onTap: object.onTap,
        )
    );
  }
}
