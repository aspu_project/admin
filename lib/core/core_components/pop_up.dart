import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'app_loader.dart';

void showLoader([Widget? loader]) => showDialog(
    context: Get.context!,
    builder: (_) =>
        PopScope(
          canPop: false,
          child: Center(
            child: loader ?? const AppLoader(),
          ),
        )
);

void showSnackBar(String label) => ScaffoldMessenger.of(Get.context!).
showSnackBar(SnackBar(
  content: Text(label),
));