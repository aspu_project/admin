
class LottieConst {
  static const _lottiePath = "assets/lottie";
  static const loader = '$_lottiePath/loader.json';
  static const messageLoader = '$_lottiePath/message_loader.json';
}

class ImageConst {
  static const _imagesPath = "assets/images";
  static const aspu = '$_imagesPath/aspu.png';
}