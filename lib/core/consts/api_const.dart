
class ApiConst {

  static const _baseUrl = 'http://192.168.137.1:8000/api';
  static const login = '$_baseUrl/login';
  static const information = '$_baseUrl/information';
  static const courses = '$_baseUrl/courses';
  static const report = '$_baseUrl/reports';
  static const students = '$_baseUrl/students';

  static String getCoursesById(int id) => '$courses/$id';

  static String getStudentsById(int id) => '$students/$id';

}