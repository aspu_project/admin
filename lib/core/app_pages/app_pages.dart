import 'package:get/get.dart';

import '../../modules/auth/presentation/screens/login_screen.dart';
import '../../modules/courses/presentation/screens/all_courses_screen.dart';
import '../../modules/courses/presentation/screens/course_screen.dart';
import '../../modules/information/presentation/screens/information_screen.dart';
import '../../modules/reports/presentation/screens/information_screen.dart';
import '../../modules/students/presentation/screens/student_by_id_screen.dart';
import '../../modules/students/presentation/screens/students_screen.dart';



class AppPages {
  static List<GetPage> get pages => [
    LoginScreen.page,
    InformationScreen.page,
    AllCoursesScreen.page,
    CourseScreen.page,
    StudentsScreen.page,
    StudentsByIdScreen.page,
    ReportsScreen.page
  ];
}
